
noTfunction=False
napisEnd1="To nie jest funkcja w zapisie ogólnej"
help="""Pomoc
To jest program do kalkulowania pierwiastka przez metodę stycznych
jako dane wystarczy wprowadzić tylko wzór funkji w postaci ogólnej
np. f(x)=700x^3+1100x^2-1400x-760, g( x )= 25x - 10 lub y=10z
ważne- program liczy tylko metodą stycznych- wszelkie logarytmy, pierwiastki i mnożenia 
muszą być policzone przed wprowadzeniem danych
znak potęgi to ^
by wyjść wpisz exit bądż EXIT
by wyświetlić ponownie pomoc wpisz help bądż HELP
"""

def CheckForEq(f):
    i = f.find('=')
    if i >= 0:
        t = f.split('=', maxsplit=1)
        if '=' in t[1] or (len(t[0]) != 1 and len(t[0]) != 4) or ('(' in t[1] or ')' in t[1]):
            return -1
        else:
            return t[1]
    else:
        return f

def CutStringForFun(tab,f,letter=-999):
    #tab={-1:0}
    #f="test"
    #print(f)
    k=-1
    nr1,nr2=0,0
    for i in range(len(f)):
        if f[i].isalpha()==True:
            k=i
            break
    if len(f)!=0 and k!=-1:
        if letter==-999:
            letter=f[k]
        elif letter!=f[k]:
            noTfunction=True
            tab[-9999]=9999
            return True
        t=f.split(letter,maxsplit=1)
        if(len(t[0])==0):
            nr1=1
        elif t[0][0]=='-':
            nr1=-1
            t[0]=t[0].lstrip('-')
            if len(t[0]) != 0:
                nr1 *= int(t[0])
        elif t[0][0]=='+':
            nr1=1
            t[0] = t[0].lstrip('+')
            if len(t[0])!=0:
                    nr1*=int(t[0])
        else:
            nr1 = int(t[0])
        if k!=len(f)-1 and t[1][0]=='^':
            if t[1][1]=='-':
                nr2=-1
                t[1]=t[1].lstrip('-')
                nr2*=int(t[1][2])
                ForNext=t[1].split(t[1][2],maxsplit=1)
                t[1]=ForNext[1]
            else:
                nr2=int(t[1][1])
            tab.update({nr2:nr1})
            ForNext = t[1].split(t[1][1], maxsplit=1)
            t[1] = ForNext[1]
        else:
            tab.update({1:nr1})
        CutStringForFun(tab, str(t[1]),letter)
    elif len(f)!=0:
        if f[0]=="-":
            nr1=-1
            f = f.lstrip('-')
        elif f[0]=='+':
            nr1=1
            f = f.lstrip('+')
        nr1*=int(f)
        tab.update({0:nr1})
    return tab

def Calculatepoch(tab):
    tab2={9999:0}
    for x in list(tab):

        if x==-9999 or x==0:
            continue
        tab2[x-1]=tab[x]*x
    return tab2

def CalByStick(tab,tab2,x=1):
    count1,count2=0,0
    for y in list(tab):
        if(y!=-9999):
            count1+=pow(x,y)*tab[y]
    for y in list(tab2):
        if(y!=9999):
            count2+=pow(x,y)*tab2[y]
    x2=x-count1/count2
    if abs(x-x2)>=0.001:
        return CalByStick(tab,tab2,x2)
    else:
        return x2

print ("Witaj")
print(help)
while (True):
    f=(input("Wpisz funkcję w wersji ogólnej albo wpisz 'EXIT' by wyjść \n"))
    f=f.replace(' ','')
    f=f.lower()
    if f=='EXIT' or f=='exit':
        break
    elif f=='HELP' or f=='help':
        print(help)
        continue
    #print(f)
    f=CheckForEq(f)
    if f==-1 or f.isalpha():
        print(napisEnd1)
        continue
    #print(f)
    tab={-9999:0}
    tab=CutStringForFun(tab,f)
    if tab[-9999]==9999:
       print(napisEnd1)
       continue
    #print(tab)
    tab2=Calculatepoch(tab)
    #print(tab2)
    wynik=CalByStick(tab,tab2)
    print("X wynosi: ",wynik)